﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Pawnshop.Startup))]
namespace Pawnshop
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
